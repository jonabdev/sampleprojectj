-- -----------------------------------------------------
-- Database Schema
-- -----------------------------------------------------

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema oneck_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sampleproject_db` DEFAULT CHARACTER SET utf8mb4 ;
USE `sampleproject_db` ;


-- -----------------------------------------------------
-- Table `tb_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_user` ;

CREATE TABLE IF NOT EXISTS `tb_user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(256) NOT NULL,
  `password_hash` VARCHAR(256) NOT NULL,


  `first_name` VARCHAR(100) NULL DEFAULT NULL,
  `last_name` VARCHAR(100) NULL DEFAULT NULL,

  `active` BIT(1) NOT NULL,
  `insert_date` TIMESTAMP NOT NULL,
  `insert_username` VARCHAR(100) NOT NULL,
  `update_date` TIMESTAMP NULL DEFAULT NULL,
  `update_username` VARCHAR(100) NULL DEFAULT NULL,
  `row_version`INT NULL,
  
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;
  
-- -----------------------------------------------------
-- Table `tb_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_role` ;

CREATE TABLE IF NOT EXISTS `tb_role` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  
  `active` BIT(1) NOT NULL,
  `insert_date` TIMESTAMP NOT NULL,
  `insert_username` VARCHAR(100) NOT NULL,
  `update_date` TIMESTAMP NULL DEFAULT NULL,
  `update_username` VARCHAR(100) NULL,
  `row_version`INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;
  
-- -----------------------------------------------------
-- Table `tb_user_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_user_role` ;

CREATE TABLE IF NOT EXISTS `tb_user_role` (
  `role_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`role_id`, `user_id`),

  INDEX `fk_tb_user_role_tb_role1` (`role_id` ASC),
  INDEX `fk_tb_user_role_tb_user1` (`user_id` ASC),

  CONSTRAINT `fk_tb_user_role_tb_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `tb_role` (`id`),
  CONSTRAINT `fk_tb_user_role_tb_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `tb_user` (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;
  
-- -----------------------------------------------------
-- Table `tb_employee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_employee` ;

CREATE TABLE IF NOT EXISTS `tb_employee` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(256) NOT NULL,
  `address` VARCHAR(256) NULL,
  `position` VARCHAR(256) NULL,
  
  `department_id` INT(11) NULL,
  
  `active` BIT(1) NOT NULL,
  `insert_date` TIMESTAMP NOT NULL,
  `insert_username` VARCHAR(100) NOT NULL,
  `update_date` TIMESTAMP NULL DEFAULT NULL,
  `update_username` VARCHAR(100) NULL,
  `row_version`INT NULL,
  
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tb_employee_tb_department1`
    FOREIGN KEY (`department_id`)
    REFERENCES `tb_department` (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;
  
  -- -----------------------------------------------------
-- Table `tb_department`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_department` ;

CREATE TABLE IF NOT EXISTS `tb_department` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(256) NOT NULL,
  
  `active` BIT(1) NOT NULL,
  `insert_date` TIMESTAMP NOT NULL,
  `insert_username` VARCHAR(100) NOT NULL,
  `update_date` TIMESTAMP NULL DEFAULT NULL,
  `update_username` VARCHAR(100) NULL,
  `row_version`INT NULL,
  
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
