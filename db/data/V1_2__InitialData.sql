-- INITIAL REQUIRED DATA 


-- -----------------------------------------------------
-- User Role (Simple user and Admin)
-- -----------------------------------------------------
/*
  ADMIN
  USER
 */
INSERT INTO `tb_role` (`id`, `name`, `active`, `insert_date`, `insert_username`, `row_version`) VALUES (1, 'ADMIN', true, CURRENT_TIMESTAMP(), 'webapp-admin', 0);
INSERT INTO `tb_role` (`id`, `name`, `active`, `insert_date`, `insert_username`, `row_version`) VALUES (2, 'USER', true, CURRENT_TIMESTAMP(), 'webapp-admin', 0);

-- -----------------------------------------------------
-- Departments Table
-- -----------------------------------------------------

INSERT INTO `tb_department` (`id`, `name`, `active`, `insert_date`, `insert_username`, `row_version`) VALUES (1, 'DevOps', true, CURRENT_TIMESTAMP(), 'webapp-admin', 0);
INSERT INTO `tb_department` (`id`, `name`, `active`, `insert_date`, `insert_username`, `row_version`) VALUES (2, 'Finance', true, CURRENT_TIMESTAMP(), 'webapp-admin', 0);
INSERT INTO `tb_department` (`id`, `name`, `active`, `insert_date`, `insert_username`, `row_version`) VALUES (3, 'Human Resources', true, CURRENT_TIMESTAMP(), 'webapp-admin', 0);

-- -----------------------------------------------------
-- Employees Table
-- -----------------------------------------------------

INSERT INTO `tb_employee` (`id`, `name`, `address`, `position`, `department_id`, `active`, `insert_date`, `insert_username`, `row_version`) 
VALUES (1, 'Jona Borishi', 'Rr.Shyqyri Ishmi, Tirane', 'Java EE Developer', 1, true, CURRENT_TIMESTAMP(), 'webapp-admin', 0);


-- .... add more info into if that is possible
