
****** REQUIREMENTS ******

Compiler - IDE : IntelliJ
Database - MySql

Server - Tomcat 8.5+
Build - Maven - 3.5+
Node - Installation Link:  https://nodejs.org/en/ (requires restart device after install)


****** GULP ******

Open terminal on the project path .../sampleprojectj

Run : 
npm install
npm install --save-dev gulp



****** Tomcat Configuration ******

<!-- ENABLE SECURE CONNECTION / server.xml -->

<Connector  port="8443" 
                protocol="org.apache.coyote.http11.Http11NioProtocol"
                maxThreads="150" 
                SSLEnabled="true"
                scheme="https" 
                secure="true"
                clientAuth="false"
                sslProtocol="TLS"
                keystoreFile="/Users/user/.keystore" keystorePass="laptopi5" />
				
(.keystore file is located in the project dir)

<!-- DATASOURCE (LOCAL) / context.xml -->
	<Resource 	name="jdbc/sampleproject_db" 
				auth="Container"
				type="javax.sql.DataSource" 
				username="root" 
				password="laptopi5"
				driverClassName="com.mysql.jdbc.Driver"
				url="jdbc:mysql://localhost:3306/sampleproject_db" 
				maxActive="15"
				
				
				
****** Database Configuration ******

Initialize DB - Scripts are located in ../sampleprojectj/db/schema ; ../sampleprojectj/db/db/data

*Change MySql root password on : pom.xml ; test/application.properties
