$(document).ready(function () {

    $('#area-loading-stellar_accounts').html(Loading.getLoadingTemplateAdmin());

    $("#seasonal-account-form").submit(function (event) {
        event.preventDefault();

        Loading.show();
        var formData = JSON.stringify($(this).serializeToJSON());

        $.ajax({
            url: window.location + '/addSeasonalAccount',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: "application/json",
            processData: false
        }).then(function (response) {

            if (response.code === ResultCode.OPERATION_CODE_SUCCESS) {
                showSuccessMessage(response.message);
                $('#close-button_success').on('click', function (event) {
                    event.preventDefault();
                    resetStellarAccounts();
                });
                resetValues();
            } else if (response.code === ResultCode.OPERATION_CODE_WARNING) {
                showErrorMessage(response.message);
            } else if (response.code === ResultCode.OPERATION_CODE_ERROR) {
                showErrorMessage(response.message);
            } else {
                showErrorMessage(response.message);
            }
            Loading.hide();
        }).catch(function (jqXHR) {
            showErrorMessage(jqXHR.responseJSON["message"]);
            Loading.hide();
        });
    });
});
function showErrorFor(message) {
    $('#profile-update-error').addClass('text-danger').html(message); //Error message
}

function disableControls() {
    FormControls.disableButton('profile-update-submit', 'loading...');
    Loading.show();
}

function enableControls() {
    FormControls.enableButton('profile-update-submit', 'Update');
    Loading.hide();
}