var RestService = {
    httpMethod: {GET: 'get', POST: 'post'},

    /**
     * User Registration
     */
    userLogin: function (data) {
        return this.prepareRequest(this.httpMethod.POST, 'user/login', null, data);
    },

    /**
     * User Registration
     */
    userRegistration: function (data) {
        return this.prepareRequest(this.httpMethod.POST, 'user/register', null, data);
    },

    /**
     * User Logout
     */
    userLogout: function () {
        return this.prepareRequest(this.httpMethod.GET, 'logout', null, null);
    },

    /**
     * Update User Profile
     */
    updateProfile: function (data) {
        return this.prepareRequest(this.httpMethod.POST, 'account/profile', 'json', data);
    },

    /**
     * Add New Employee
     */
    addEmployee: function (data) {
        return this.prepareRequest(this.httpMethod.POST, 'manage/add-employee', 'json', data);
    },

    /**
     * Show Employee Table
     */
    getEmployeeList: function () {
        return this.prepareRequest(this.httpMethod.GET, 'manage/employee-list', null, null);
    },

    /**
     * Edit Employee
     */
    editEmployee: function (data) {
        return this.prepareRequest(this.httpMethod.POST, 'manage/edit-employee', 'json', data);
    },

    /**
     * Delete Employee
     */
    deleteEmployee: function (data) {
        return this.prepareRequest(this.httpMethod.POST, 'manage/delete-employee', null, data);
    },

    /**
     * Prepare Request
     */
    prepareRequest: function (method, url, contentType, data) {
        var requestConfig = null;

        url = BASE_URL + url;

        if (method === this.httpMethod.POST) {
            requestConfig = {
                method: method,
                data: data,
                headers: this.prepareRequestHeaders(contentType)
            };
        }
        if (method === this.httpMethod.GET) {
            requestConfig = {
                method: method,
                params: data
            };
        }
        return axios(url, (requestConfig || {}));
    },

    /**
     * Prepare Request Headers
     */
    prepareRequestHeaders: function (contentType) {
        if (contentType === 'json') {
            return {'Content-Type': 'application/json'}
        } else {
            return {'Content-Type': 'application/x-www-form-urlencoded'}
        }
    },

    isValidResponseData: function (response) {
        return response !== undefined
            && response !== null
            && response.data !== undefined
            && response.data !== null
            && response.data.code !== undefined
            && response.data.code !== null;
    },

    getResponseStatusCode: function (response) {
        return response.data.code;
    },

    OperationResult: {
        OPERATION_CODE_SUCCESS: 0,
        OPERATION_CODE_ERROR: 1,
        OPERATION_CODE_WARNING: 2
    }
};

