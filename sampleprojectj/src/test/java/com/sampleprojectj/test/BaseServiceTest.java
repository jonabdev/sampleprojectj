package com.sampleprojectj.test;

import com.github.javafaker.Faker;

public abstract class BaseServiceTest {

    private Faker faker;

    public Faker getFaker() {
        if (faker == null) {
            faker = new Faker();
        }
        return faker;
    }

}
