package com.sampleprojectj.test;

import com.sampleprojectj.core.config.ApplicationConfig;
import com.sampleprojectj.core.entity.user.User;
import com.sampleprojectj.core.enumerator.RoleType;
import com.sampleprojectj.core.exception.ServiceException;
import com.sampleprojectj.core.service.user.UserService;
import com.github.javafaker.Faker;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class, RepositoryConfigTest.class})
public class UserServiceTest extends BaseServiceTest {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserService userService;

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void user_login() throws ServiceException {
        Optional<User> user = userService.findAll().stream().findAny();
        if (user.isPresent()) {
            Optional<User> userDetails = userService.getUserByEmail(user.get().getEmail());
            assertThat(userDetails, is(notNullValue()));
        }
    }

    @Test
    public void save_user_data() throws ServiceException {
        Faker faker = getFaker();
        User user = new User();
        user.setEmail(faker.internet().emailAddress());
        user.setPassword(faker.internet().password());

        List<RoleType> roleTypeList = new ArrayList<>();
        roleTypeList.add(RoleType.USER);

        user.setActive(true);
        user.setInsertDate(Calendar.getInstance());
        user.setInsertUsername("sample-project-setup");

        User userInserted = userService.save(user, roleTypeList);

        assertThat(userInserted, is(notNullValue()));
        assertThat(userInserted.getId(), is(notNullValue()));
    }

    @Test
    public void remove_user() throws ServiceException {
        Optional<User> user = userService.findAll().stream().findAny();
        if (user.isPresent()) {
            User userSelected = user.get();
            userSelected.setActive(false);
            userSelected.setUpdateDate(Calendar.getInstance());
            userSelected.setUpdateUsername("testCase");
            userService.save(user.get());
        }
    }

    @Test
    public void recover_user_password() {

    }

    @Test
    public void create_admin_account() throws ServiceException {
        Faker faker = getFaker();
        User user = new User();
        user.setEmail("jborishi@gmail.com");
        String password = bCryptPasswordEncoder.encode("Laptopi5");
        user.setPassword(password);

        List<RoleType> roleTypeList = new ArrayList<>();
        roleTypeList.add(RoleType.ADMIN);

        user.setActive(true);
        user.setInsertDate(Calendar.getInstance());
        user.setInsertUsername(faker.internet().emailAddress());

        user = userService.save(user, roleTypeList);
        assertThat(user, is(notNullValue()));
    }

    @Test
    public void encode_password() throws ServiceException {
        String password = bCryptPasswordEncoder.encode("Laptopi5");
        System.out.println("Encoded password is: "+password);
    }
}