$(document).ready(function () {

    $('#loading-add-employee').html(Loading.getLoadingTemplateAdmin());
    $('#loading-edit-employee').html(Loading.getLoadingTemplateAdmin());
    enableControls('create');
    enableControls('edit');

    /**
     * Create employee form
     */
    $("#frmAddEmployee").submit(function (event) {
        event.preventDefault();
        $('#add-employee-error').empty();

        disableControls('create');

        var postData = JSON.stringify($(this).serializeToJSON());
        RestService
            .addEmployee(postData)
            .then(function (response) {

                var statusCode = RestService.getResponseStatusCode(response);
                var data = response.data;

                if (statusCode == RestService.OperationResult.OPERATION_CODE_SUCCESS) {
                    showSuccessModal(data.detail);
                    $('.close-button_success').on('click', function (event) {
                        event.preventDefault();
                        resetEmployeeList();
                    });
                    resetValues();
                } else if (statusCode == RestService.OperationResult.OPERATION_CODE_WARNING) {
                    showErrorOnEmployeeCreate(data.detail);
                } else if (statusCode == RestService.OperationResult.OPERATION_CODE_ERROR) {
                    showErrorOnEmployeeCreate(data.detail);
                } else {
                    showErrorOnEmployeeCreate('Invalid return for create employee operation!');
                }
                enableControls('create');
            }).catch(function (error) {
            showErrorOnEmployeeCreate('Error processing request. ' + error.message);
            enableControls('create');
        });
    });

    /**
     * Edit employee form
     */
    $("#frmEditEmployee").submit(function (event) {
        event.preventDefault();
        $('#edit-employee-error').empty();

        disableControls('edit');

        var postData = JSON.stringify($(this).serializeToJSON());
        RestService
            .editEmployee(postData)
            .then(function (response) {

                var statusCode = RestService.getResponseStatusCode(response);
                var data = response.data;

                if (statusCode == RestService.OperationResult.OPERATION_CODE_SUCCESS) {
                    showSuccessModal(data.detail);
                    $('.close-button_success').on('click', function (event) {
                        event.preventDefault();
                        window.location.href = BASE_URL + 'manage/manage-employees';
                    });
                } else if (statusCode == RestService.OperationResult.OPERATION_CODE_WARNING) {
                    showErrorOnEmployeeUpdate(data.detail);
                } else if (statusCode == RestService.OperationResult.OPERATION_CODE_ERROR) {
                    showErrorOnEmployeeUpdate(data.detail);
                } else {
                    showErrorOnEmployeeUpdate('Invalid return for edit employee operation!');
                }
                enableControls('edit');
            }).catch(function (error) {
            showErrorOnEmployeeUpdate('Error processing request. ' + error.message);
            enableControls('edit');
        });
    });

    /**
     * Delete employee operation
     */
    $('#confirm-btn').on('click', function (event) {
        event.preventDefault();

        var formData = "employeeId=" + $('#employeeId').val();
        RestService
            .deleteEmployee(formData)
            .then(function (response) {

                var statusCode = RestService.getResponseStatusCode(response);
                var data = response.data;

                if (statusCode == RestService.OperationResult.OPERATION_CODE_SUCCESS) {
                    Modal.hide("#confirmModal");
                    showSuccessModal(data.detail);

                    $('.close-button_success').on('click', function (event) {
                        event.preventDefault();
                        resetEmployeeList();
                    });
                    resetValues();
                } else if (statusCode == RestService.OperationResult.OPERATION_CODE_WARNING) {
                    showErrorOnEmployeeDelete(data.detail);
                } else if (statusCode == RestService.OperationResult.OPERATION_CODE_ERROR) {
                    showErrorOnEmployeeDelete(data.detail);
                } else {
                    showErrorOnEmployeeDelete('Invalid return for delete employee operation!');
                }
            }).catch(function (error) {
            showErrorOnEmployeeDelete('Error processing request. ' + error.message);
        });
    });

});

/**
 * Delete employee confirm modal
 */
$(document).on("click", '.delete-employee-link', function (event) {
    event.preventDefault();
    var employeeId = $(this).attr('data');
    $('#employeeId').val(employeeId);

    var confirm_message = "Are you sure you want to delete employee?";
    showConfirmModal(confirm_message);
});

function resetValues() {
    $('#employeeName').val('');
    $('#employeeAddress').val('');
    $('#employeePosition').val('');
    $('#employeeDepartment').val("0");
    $('#employeeActive').prop('checked', true)
}

function resetEmployeeList() {
    RestService
        .getEmployeeList()
        .then(function (response) {
            $('#employees-list').empty().html(response.data);
        }).catch(function (error) {
    });
}

function showErrorOnEmployeeCreate(message) {
    $('#add-employee-error').addClass('text-danger').html(message); //Error message
}

function showErrorOnEmployeeUpdate(message) {
    $('#edit-employee-error').addClass('text-danger').html(message); //Error message
}

function showErrorOnEmployeeDelete(message) {
    $('#confirm-error').addClass('text-danger').html(message); //Error message
}

function disableControls(type) {
    if (type === 'create') {
        FormControls.disableButton('add-employee-submit', 'Loading...');
        Loading.showById('#loading-add-employee');
    }
    if (type === 'edit') {
        FormControls.disableButton('edit-employee-submit', 'Loading...');
        Loading.showById('#loading-edit-employee');
    }
}

function enableControls(type) {
    if (type === 'create') {
        FormControls.enableButton('add-employee-submit', 'Add Employee');
        Loading.hideById('#loading-add-employee');
    }
    if (type === 'edit') {
        FormControls.enableButton('edit-employee-submit', 'Edit Employee');
        Loading.hideById('#loading-add-employee');
    }
}


