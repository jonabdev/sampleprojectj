var FormControls = {
    disableButton: function (id, text) {
        $('#' + id).prop( "disabled", true);
        $('#' + id).addClass("disabled-element");
        if (text !== undefined && text !== null) {
            $('#' + id).html(text);
        }
    },
    enableButton: function (id, text) {
        $('#' + id).prop( "disabled", false);
        $('#' + id).removeClass("disabled-element");
        if (text !== undefined && text !== null) {
            $('#' + id).html(text);
        }
    }
};

var Loading = {
    getLoadingTemplateAdmin: function () {
        return $("#template-loading-admin").html();
    },
    show: function () {
        $('.loading-admin').fadeIn(300);
    },
    hide: function () {
        $('.loading-admin').fadeOut(300);
    },
    showById: function (id) {
        if (id !== undefined && id !== null) {
            $(id).children().show();
            $(id).fadeIn(300);
        }
    },
    hideById: function (id) {
        if (id !== undefined && id !== null) {
            $(id).children().hide();
            $(id).fadeOut(300);
        }
    }
};

var Modal = {
    show: function (id) {
        $(id).modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    },
    hide: function (id) {
        $(id).modal('hide');
    }
};

function showSuccessModal(message) {
    $("#success h5").empty().append(message);
    Modal.show("#successModal");
}

function showConfirmModal(message) {
    $("#confirm-question h5").empty().append(message);
    Modal.show("#confirmModal");
}