$(document).ready(function () {
    $('#loading-profile-update').html(Loading.getLoadingTemplateAdmin());
    enableControls();

    $("#frmUpdateUserProfile").submit(function (event) {
        event.preventDefault();
        disableControls();

        var postData = JSON.stringify($(this).serializeToJSON());
        RestService
            .updateProfile(postData)
            .then(function (response) {

                var statusCode = RestService.getResponseStatusCode(response);
                var data = response.data;

                if (statusCode == RestService.OperationResult.OPERATION_CODE_SUCCESS) {
                    $('#profile-update-error').html('');
                    showSuccessModal(data.detail);

                    var isEmailChanged = data.item;
                    if(isEmailChanged){
                        $('.close-button_success').on('click', function (event) {
                            event.preventDefault();
                            userLogout();
                            window.location.href=BASE_URL;
                        });
                    }
                } else if (statusCode == RestService.OperationResult.OPERATION_CODE_WARNING) {
                    showErrorForProfileUpdate(data.detail);
                } else if (statusCode == RestService.OperationResult.OPERATION_CODE_ERROR) {
                    showErrorForProfileUpdate(data.detail);
                } else {
                    showErrorForProfileUpdate('Invalid return for update profile operation!');
                }
                enableControls();
            })
            .catch(function (error) {
                showErrorForProfileUpdate('Error processing request. ' + error.message);
                enableControls();
            });
    });
});

function userLogout() {
    RestService
        .userLogout()
        .catch(function (error) {
        });
}

function showErrorForProfileUpdate(message) {
    $('#profile-update-error').addClass('text-danger').html(message); //Error message
}

function disableControls() {
    FormControls.disableButton('profile-update-submit', 'Loading...');
    Loading.show();
}

function enableControls() {
    FormControls.enableButton('profile-update-submit', 'Update');
    Loading.hide();
}