$(document).ready(function () {

    $('#loading-login').html(Loading.getLoadingTemplateAdmin());
    enableControls('login');
    enableControls('register');

    hideRegisterForm();
    showLoginForm();

    $('.login-function').on('click', function (event) {
        event.preventDefault();

        $('#login-error').html('');
        showLoginForm();
    });
    $('.register-function').on('click', function (event) {
        event.preventDefault();
        showRegisterForm();
    });

    /**
     * Login form
     */
    $('#frmLogin').on('submit', function (event) {
        event.preventDefault();
        disableControls('login');

        var postData = $(this).serialize();
        $('#login-error').html('');

        RestService
            .userLogin(postData)
            .then(function (response) {

                if (!RestService.isValidResponseData(response)) {
                    $('#login-error').html('Error processing login!');
                    enableControls('login');
                    return;
                }
                var statusCode = RestService.getResponseStatusCode(response);
                var data = response.data;

                if (statusCode == RestService.OperationResult.OPERATION_CODE_SUCCESS) {
                    console.log("Login was successful!");
                    window.location.replace(BASE_URL + "home");
                } else if (statusCode == RestService.OperationResult.OPERATION_CODE_ERROR) {
                    loginError(data.detail);
                } else if (statusCode == RestService.OperationResult.OPERATION_CODE_WARNING) {
                    loginError(data.detail);
                } else {
                    loginError('Invalid return for login operation!');
                }
                enableControls('login');
            })
            .catch(function (error) {
                loginError('Error processing registration. ' + error.message);
                enableControls('login');
            });
    });

    /**
     * Registration form
     */
    $('#frmRegistration').on('submit', function (event) {
        event.preventDefault();
        disableControls('register');

        var postData = $(this).serialize();
        $('#login-error').html('');

        RestService
            .userRegistration(postData)
            .then(function (response) {

                if (!RestService.isValidResponseData(response)) {
                    $('#login-error').html('Error processing registration!');
                    enableControls('register');
                    return;
                }
                var statusCode = RestService.getResponseStatusCode(response);
                var data = response.data;

                if (statusCode == RestService.OperationResult.OPERATION_CODE_SUCCESS) {
                    showSuccessModal(data.detail);
                    resetPasswordFields();
                } else if (statusCode == RestService.OperationResult.OPERATION_CODE_ERROR) {
                    loginError(data.detail);
                } else if (statusCode == RestService.OperationResult.OPERATION_CODE_WARNING) {
                    loginError(data.detail);
                } else {
                    loginError('Invalid return for register operation!');
                }
                enableControls('register');
            })
            .catch(function (error) {
                loginError('Error processing registration. ' + error.message);
                enableControls('register');
            });
    });
});

function showRegisterForm() {
    $('.loginBox').fadeOut('fast', function () {
        $('.registerBox').fadeIn('fast');
        $('.login-footer').fadeOut('fast', function () {
            $('.register-footer').fadeIn('fast');
        });
        $('.modal-title').html('Register with');
        $('#login-error').html('');
    });
    resetPasswordFields();
    $('.error').removeClass('text-danger').html('');

}

function hideRegisterForm() {
    $('.register-footer').hide();
    $('.registerBox').hide();
}

function showLoginForm() {
    $('.registerBox').fadeOut('fast', function () {
        $('.loginBox').fadeIn('fast');
        $('.register-footer').fadeOut('fast', function () {
            $('.login-footer').fadeIn('fast');
        });
        $('.modal-title').html('Login with');
    });
    resetPasswordFields();
    $('.error').removeClass('text-danger').html('');
}

function loginError(message) {
    $('.modal-dialog').addClass('shake');
    $('#login-error').addClass('text-danger').html(message); //Error message

    resetPasswordFields();
    setTimeout(function () {
        $('.modal-dialog').removeClass('shake');
    }, 1000);
}

function resetPasswordFields() {
    $('input[type="password"]').val('');
}

function disableControls(type) {
    if (type === 'login') {
        FormControls.disableButton('login-submit', 'authenticating...');
    }
    if (type === 'register') {
        FormControls.disableButton('register-submit', 'creating account...');
    }
    Loading.show();
}

function enableControls(type) {
    if (type === 'login') {
        FormControls.enableButton('login-submit', 'Login');
    }
    if (type === 'register') {
        FormControls.enableButton('register-submit', 'Register');
    }
    Loading.hide();
}
   