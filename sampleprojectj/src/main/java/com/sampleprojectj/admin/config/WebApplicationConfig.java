package com.sampleprojectj.admin.config;

import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

import java.util.Locale;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.sampleprojectj.core", "com.sampleprojectj.admin"})
public class WebApplicationConfig implements WebMvcConfigurer {

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
    }

    @Bean
    public SpringResourceTemplateResolver templateResolver() {
        //Admin
        SpringResourceTemplateResolver adminTemplateResolver = new SpringResourceTemplateResolver();
        adminTemplateResolver.setApplicationContext(applicationContext);
        adminTemplateResolver.setPrefix("/WEB-INF/views/");
        adminTemplateResolver.setSuffix(".html");
        adminTemplateResolver.setTemplateMode("HTML5");

        adminTemplateResolver.setCacheable(false);
        adminTemplateResolver.setOrder(1);
        return adminTemplateResolver;
    }

    @Bean
    public SpringTemplateEngine templateEngine() {
        SpringTemplateEngine adminTemplateEngine = new SpringTemplateEngine();
        adminTemplateEngine.setTemplateResolver(templateResolver());
        adminTemplateEngine.addDialect(new LayoutDialect());
        adminTemplateEngine.setEnableSpringELCompiler(true);
        return adminTemplateEngine;
    }

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        ThymeleafViewResolver adminResolver = new ThymeleafViewResolver();
        adminResolver.setTemplateEngine(templateEngine());
        registry.viewResolver(adminResolver);
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource adminMessageSource = new ResourceBundleMessageSource();
        adminMessageSource.setBasename("messages");
        adminMessageSource.setDefaultEncoding("UTF-8");
        return adminMessageSource;
    }

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.UK);
        return slr;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/css/**").addResourceLocations("/WEB-INF/static/css/");
        registry.addResourceHandler("/fonts/**").addResourceLocations("/WEB-INF/static/fonts/");
        registry.addResourceHandler("/img/**").addResourceLocations("/WEB-INF/static/img/");
        registry.addResourceHandler("/js/**").addResourceLocations("/WEB-INF/static/js/");
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor adminLci = new LocaleChangeInterceptor();
        adminLci.setParamName("lang");
        return adminLci;
    }

    @Override
    public void configurePathMatch(PathMatchConfigurer matcher) {
        matcher.setUseRegisteredSuffixPatternMatch(true);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }

    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }
}
