package com.sampleprojectj.admin.security.config;

import com.sampleprojectj.admin.security.constants.ApplicationSecurityConstants;
import com.sampleprojectj.core.service.user.UserService;
import com.sampleprojectj.core.util.TokenUtil;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class TokenAuthenticationFilter extends GenericFilterBean {

    private UserService userService;

    public TokenAuthenticationFilter(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        final HttpServletRequest httpRequest = (HttpServletRequest) request;

        //extract token from header
        final String authToken = httpRequest.getHeader(ApplicationSecurityConstants.TOKEN_HEADER_SUCCESS_LOGIN);

        if (StringUtils.hasText(authToken)) {
            String username = TokenUtil.getUserNameFromToken(authToken);
            UserDetails userDetails = userService.loadUserByUsername(username);

            if (TokenUtil.validateToken(authToken, userDetails)) {
                UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userDetails,
                        userDetails.getPassword(), userDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(token);
            }
        }
        chain.doFilter(request, response);
    }

}