package com.sampleprojectj.admin.security.config;

import com.sampleprojectj.core.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;

@EnableWebSecurity
@EnableGlobalMethodSecurity(jsr250Enabled = true, prePostEnabled = true)
@ComponentScan(basePackages = {"com.sampleprojectj.admin"})
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final String LOGIN_URL = "/signin";
    private static final String LOGOUT_URL = "/logout";

    @Autowired
    private UserService userService;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public TokenAuthenticationFilter tokenAuthenticationFilter() {
        return new TokenAuthenticationFilter(userService);
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler(){
        return new CustomAccessDeniedHandler();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }

    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers(LOGIN_URL).permitAll()
                    .antMatchers("/user/**").permitAll()
                    .antMatchers("/manage/**").hasAuthority("ADMIN")
                    .anyRequest().authenticated()
                .and()
                    .addFilterBefore(tokenAuthenticationFilter(), AnonymousAuthenticationFilter.class)
                        .csrf().disable()
                        .formLogin()
                        .loginPage(LOGIN_URL)
                        .usernameParameter( "email")
                        .passwordParameter("password")
                        .successHandler((request, response, exp) -> {
                            response.sendRedirect(request.getContextPath()+"/home");
                        })
                        .failureHandler((request, response, exp) -> {
                            request.setAttribute("message", "invalid user credentials");
                            response.sendRedirect(request.getContextPath() + LOGIN_URL);
                        })
                .and()
                    .exceptionHandling().accessDeniedHandler(accessDeniedHandler())
                .and()
                    .logout()
                    .logoutUrl(LOGOUT_URL)
                    .deleteCookies("JSESSIONID")
                    .logoutSuccessUrl(LOGIN_URL)

                .and()
                    .headers().frameOptions().disable()
                .and()
                    .httpBasic()
                .and()
                    .sessionManagement().maximumSessions(1);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .debug(true)
                .ignoring()
                .antMatchers("/css/**")
                .antMatchers("/js/**")
                .antMatchers("/fonts/**")
                .antMatchers("/img/**");
    }
}
