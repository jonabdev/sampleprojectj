package com.sampleprojectj.admin.security.constants;

public class ApplicationSecurityConstants {

    public static final String WWW_AUTH_HEADER = "WWW-Authenticate";

    public static final String TOKEN_HEADER_PARAM = "Authorization";
    public static final String TOKEN_URL_PARAM = "access_token";
    public static final String TOKEN_HEADER_PREFIX = "Bearer ";
    public static final String TOKEN_HEADER_SUCCESS_LOGIN = "x-auth-token";

}
