package com.sampleprojectj.admin.controller.base;

import com.sampleprojectj.admin.response.Response;
import com.sampleprojectj.core.constants.ApplicationConstants;
import com.sampleprojectj.core.helper.LogHelper;
import com.sampleprojectj.core.service.company.DepartmentService;
import com.sampleprojectj.core.service.company.EmployeeService;
import com.sampleprojectj.core.service.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.ModelAndView;


public abstract class BaseController {

    private static Logger logger = LogManager.getLogger(BaseController.class);

    public static int OPERATION_CODE_SUCCESS = 0;
    public static int OPERATION_CODE_ERROR = 1;
    public static int OPERATION_CODE_WARNING = 2;

    //region IOC

    @Autowired
    private UserService userService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private DepartmentService departmentService;

    //endregion

    public UserService getUserService() {
        return userService;
    }

    public EmployeeService getEmployeeService() {
        return employeeService;
    }

    public DepartmentService getDepartmentService() {
        return departmentService;
    }


    public void logError(Logger logger, String operation, Exception e) {
        logError(logger, operation, "An error occurred while trying to execute the operation " + operation, e);
    }

    public void logError(Logger logger, String operation, String message, Exception e) {
        LogHelper.logError(logger, operation, message, e);
    }

    public void logErrorAndSetModel(Logger logger, String operation, String message, Exception e, ModelAndView modelAndView) {
        logError(logger, operation, message, e);
        modelAndView.addObject("error", message);
    }

    public <T> ResponseEntity<Response<T>> prepareResponseEntityWithData(int code, String message, HttpStatus httpStatus) {
        return prepareResponseEntityWithData(code, message, null, null, httpStatus);
    }

    public <T> ResponseEntity<Response<T>> prepareResponseEntityWithData(int code, String message, String detail, HttpStatus httpStatus) {
        return prepareResponseEntityWithData(code, message, detail, null, httpStatus);
    }

    public <T> ResponseEntity<Response<T>> prepareResponseEntityWithData(int code, String message, String detail, T item, HttpStatus httpStatus) {
        Response response = new Response();

        response.setCode(code);
        response.setMessage(message);
        response.setDetail(detail);

        response.setItem(item);

        return new ResponseEntity(response, httpStatus);
    }

    public <T> ResponseEntity<Response<T>> logAndPrepareResponseEntityForException(Logger logger, String method, Exception e) {
        logError(logger, method, e);
        Response response = new Response();
        response.setCode(OPERATION_CODE_ERROR);
        if(StringUtils.isEmpty(e.getMessage())){
            response.setMessage(ApplicationConstants.GENERIC_ERROR);
        }else{
            response.setMessage(e.getMessage());
        }
        return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
