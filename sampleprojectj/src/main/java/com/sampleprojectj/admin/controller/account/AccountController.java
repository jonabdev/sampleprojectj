package com.sampleprojectj.admin.controller.account;

import com.sampleprojectj.admin.controller.base.BaseController;
import com.sampleprojectj.admin.response.Response;
import com.sampleprojectj.core.constants.ApplicationConstants;
import com.sampleprojectj.core.dto.UserProfileDTO;
import com.sampleprojectj.core.entity.user.User;
import com.sampleprojectj.core.exception.ServiceException;
import com.sampleprojectj.core.helper.DataHelper;
import com.sampleprojectj.core.mapper.account.UserProfileMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Controller
@RequestMapping("/account")
public class AccountController extends BaseController {
    private static Logger logger = LogManager.getLogger(AccountController.class);

    //region VIEWS
    private static final String VIEW_PROFILE = "account/profile";
    //endregion

    //region GET
    @GetMapping("/profile")
    public ModelAndView login(@AuthenticationPrincipal User user) {
        ModelAndView modelAndView = new ModelAndView(VIEW_PROFILE);

        Optional<User> userFromDb = getUserService().findById(user.getId());
        if (userFromDb.isPresent()) {
            modelAndView.addObject("profile", userFromDb.get());
        }
        modelAndView.addObject("active_page_profile", "active");
        return modelAndView;
    }
    //endregion

    //region POST
    @PostMapping("/profile")
    @ResponseBody
    public ResponseEntity<Response<Boolean>> updateProfile(@RequestBody UserProfileDTO userProfileDTO,
                                                           @AuthenticationPrincipal User user) {

        if (!DataHelper.validateUserProfileData(userProfileDTO)) {
            return prepareResponseEntityWithData(OPERATION_CODE_WARNING,
                    "User information is incomplete.",
                    "Please enter the required user information before proceeding.",
                    HttpStatus.OK);
        }
        if (!DataHelper.isEmailValid(userProfileDTO.getEmail())) {
            return prepareResponseEntityWithData(OPERATION_CODE_WARNING,
                    "User information is incomplete.",
                    "Please enter a valid e-mail address before proceeding.",
                    HttpStatus.OK);
        }
        // CHECK EMAIL
        Optional<User> userByEmail = getUserService().getUserByEmail(userProfileDTO.getEmail());
        if (userByEmail.isPresent() && !userByEmail.get().getId().equals(user.getId())) {
            return prepareResponseEntityWithData(OPERATION_CODE_WARNING,
                    "Email already exists!",
                    "The email address already exists in our database! Please, try again.",
                    HttpStatus.OK);
        }
        try {

            Optional<User> userFromDb = getUserService().findById(user.getId());
            boolean isEmailUpdated = false;
            if (!userFromDb.get().getEmail().equals(userProfileDTO.getEmail())) {
                isEmailUpdated = true;
            }
            UserProfileMapper userProfileMapper = new UserProfileMapper(userFromDb.get());

            User userToUpdate = userProfileMapper.map(userProfileDTO);
            getUserService().updateUserInformation(userToUpdate);

            return prepareResponseEntityWithData(OPERATION_CODE_SUCCESS,
                    "Profile updated successfully!",
                    "User profile has been updated successfully!",
                    isEmailUpdated,
                    HttpStatus.OK);

        } catch (ServiceException e) {
            logError(logger, "updateProfile", e);
            return prepareResponseEntityWithData(OPERATION_CODE_ERROR,
                    e.getMessage(),
                    ApplicationConstants.GENERIC_ERROR,
                    HttpStatus.OK);

        } catch (Exception e) {
            return logAndPrepareResponseEntityForException(logger, "updateProfile", e);
        }
    }
    //endregion
}
