package com.sampleprojectj.admin.controller;

import com.sampleprojectj.admin.controller.base.BaseController;
import com.sampleprojectj.core.entity.user.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController extends BaseController {

    private static Logger logger = LogManager.getLogger(HomeController.class);

    //region VIEWS
    private static final String VIEW_LOGIN = "user/login";
    private static final String VIEW_HOME = "home/index";
    //endregion

    //region GET
    @GetMapping("/signin")
    public ModelAndView signin() {

        ModelAndView modelAndView = new ModelAndView(VIEW_LOGIN);
        modelAndView.addObject("isLogin", true);
        return modelAndView;
    }

    @GetMapping("/logout")
    @ResponseBody
    public ResponseEntity<Boolean> logout(@AuthenticationPrincipal final User user) {
        if(user == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
    }

    @GetMapping(value = {"/", "home"})
    public ModelAndView home(@AuthenticationPrincipal final User user) {
        ModelAndView modelAndView = new ModelAndView(VIEW_LOGIN);
        if (user != null) {
            modelAndView = new ModelAndView(VIEW_HOME);
            modelAndView.addObject("userName", user.getUsername());
            modelAndView.addObject("userRoles", user.getAuthorities());
            modelAndView.addObject("active_page_home", "active");
        } else {
            modelAndView.addObject("isLogin", true);
        }
        return modelAndView;
    }
    //endregion

    //region POST
    //endregion
}
