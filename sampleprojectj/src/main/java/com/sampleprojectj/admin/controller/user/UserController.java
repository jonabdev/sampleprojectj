package com.sampleprojectj.admin.controller.user;

import com.sampleprojectj.admin.controller.base.BaseController;
import com.sampleprojectj.admin.response.Response;
import com.sampleprojectj.core.constants.ApplicationConstants;
import com.sampleprojectj.core.entity.user.User;

import com.sampleprojectj.core.enumerator.RoleType;
import com.sampleprojectj.core.exception.ServiceException;
import com.sampleprojectj.core.helper.DataHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/user")
public class UserController extends BaseController {
    private static Logger logger = LogManager.getLogger(UserController.class);

    //region VIEWS
    //endregion

    //region IOC
    @Autowired
    private AuthenticationManager authenticationManager;
    //endregion

    //region GET
    //end region

    //region POST
    @PostMapping("/login")
    @ResponseBody
    public ResponseEntity<Response<String>> login(@RequestParam("email") String email,
                                                  @RequestParam("password") String password) {
        try {
            if(!DataHelper.validateUserLoginData(email, password)){
                return prepareResponseEntityWithData(OPERATION_CODE_WARNING,
                        "User information is incomplete.",
                        "Please enter e-mail address and password before proceeding.",
                        HttpStatus.OK);
            }
            Optional<User> user = getUserService().login(email, password);

            if (!user.isPresent()) {
                return prepareResponseEntityWithData(OPERATION_CODE_ERROR,
                        ApplicationConstants.SECURITY_GENERIC_ERROR,
                        "User name or password is incorrect, please try again.",
                        HttpStatus.OK);
            }

            //Authenticate User
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(email, password, user.get().getAuthorities());
            Authentication authentication = this.authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);

            return prepareResponseEntityWithData(OPERATION_CODE_SUCCESS,
                    "User logged successfully",
                    "User logged successfully!",
                    HttpStatus.OK);

        } catch (InsufficientAuthenticationException e) {
            logError(logger, "login", e);
            return prepareResponseEntityWithData(OPERATION_CODE_ERROR,
                    "User has no access rights. Please contact website administrator.",
                    HttpStatus.OK);

        } catch (Exception e) {
            return logAndPrepareResponseEntityForException(logger, "login", e);
        }
    }

    @PostMapping("/register")
    @ResponseBody
    public ResponseEntity<Response<String>> registration(@RequestParam("email") final Optional<String> email,
                                                         @RequestParam("password") final Optional<String> password,
                                                         @RequestParam("password_confirm") final Optional<String> confirmPassword) {
        try {

            // VALIDATE EMAIL
            if (!email.isPresent()) {
                return prepareResponseEntityWithData(OPERATION_CODE_WARNING,
                        "User registration data is incomplete.",
                        "Please enter an e-mail address before proceeding.",
                        HttpStatus.OK);
            }
            if (!DataHelper.isEmailValid(email.get())) {
                return prepareResponseEntityWithData(OPERATION_CODE_WARNING,
                        "User registration data is incomplete.",
                        "Please enter a valid e-mail address before proceeding.",
                        HttpStatus.OK);
            }

            // CHECK EMAIL
            Optional<User> userByEmail = getUserService().getUserByEmail(email.get());
            if (userByEmail.isPresent()) {
                return prepareResponseEntityWithData(OPERATION_CODE_WARNING,
                        "Email submitted already exists!",
                        "The email submitted already exists in our database! Please, try again.",
                        HttpStatus.OK);
            }

            ResponseEntity responseEntity = validateDataForPasswordOperation(password, confirmPassword);
            if (responseEntity != null) {
                return responseEntity;
            }

            // CREATE USER - SIMPLE USER
            User newUser = new User();
            newUser.setEmail(email.get().toLowerCase());
            newUser.setPassword(password.get());

            List<RoleType> rolesList = new ArrayList<>();
            rolesList.add(RoleType.USER);

            getUserService().save(newUser, rolesList);

            return prepareResponseEntityWithData(OPERATION_CODE_SUCCESS,
                    "User registered successfully!",
                    "User has been registered successfully!",
                    HttpStatus.CREATED);

        } catch (ServiceException e) {
            logError(logger, "registration", e);
            return prepareResponseEntityWithData(OPERATION_CODE_ERROR,
                    ApplicationConstants.SECURITY_INVALID_PARAMETERS_ERROR,
                    HttpStatus.OK);

        } catch (Exception e) {
            return logAndPrepareResponseEntityForException(logger, "registration", e);
        }
    }

    //endregion
    private ResponseEntity<Response<String>> validateDataForPasswordOperation(Optional<String> newPassword,
                                                                              Optional<String> confirmPassword) {

        if (!(newPassword.isPresent() && confirmPassword.isPresent())) {
            logger.error("Invalid user request. There is no valid current and new password");
            return prepareResponseEntityWithData(OPERATION_CODE_WARNING,
                    "There is no password data",
                    "Please enter the password and confirm password before proceeding.",
                    HttpStatus.OK);
        }

        if (!DataHelper.isPasswordValid(newPassword.get())) {
            return prepareResponseEntityWithData(OPERATION_CODE_WARNING,
                    "Password data does not match to website policy",
                    "Password must contain between 8 and 20 characters, one digit and one upper case letter.",
                    HttpStatus.OK);
        }

        if (!(newPassword.equals(confirmPassword))) {
            return prepareResponseEntityWithData(OPERATION_CODE_WARNING,
                    "Password data is not valid",
                    "New password and confirm password do not match, please try again.",
                    HttpStatus.OK);
        }
        return null;
    }
}
