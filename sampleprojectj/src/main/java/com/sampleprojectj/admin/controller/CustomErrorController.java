package com.sampleprojectj.admin.controller;

import com.sampleprojectj.admin.controller.base.BaseController;;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CustomErrorController extends BaseController {

    private static Logger logger = LogManager.getLogger(CustomErrorController.class);

    //region VIEWS
    private static final String VIEW_ACCESS_DENIED = "error/403";
    //endregion

    //region GET
    @GetMapping("/403")
    public ModelAndView notFound() {
        return new ModelAndView(VIEW_ACCESS_DENIED);
    }
    //end region

    //region POST
    //end region
}
