package com.sampleprojectj.admin.controller.company;

import com.sampleprojectj.admin.controller.base.BaseController;
import com.sampleprojectj.admin.response.Response;
import com.sampleprojectj.core.dto.EmployeeDTO;
import com.sampleprojectj.core.entity.company.Department;
import com.sampleprojectj.core.entity.company.Employee;
import com.sampleprojectj.core.exception.ServiceException;
import com.sampleprojectj.core.helper.DataHelper;
import com.sampleprojectj.core.mapper.company.EmployeeDataMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/manage")
public class CompanyController extends BaseController {
    private static Logger logger = LogManager.getLogger(CompanyController.class);

    //region VIEWS
    private static final String VIEW_MANAGE_EMPLOYEES = "company/manage-employees";
    private static final String VIEW_EDIT_EMPLOYEE = "company/edit-employee";
    private static final String VIEW_EMPLOYEES_TABLE = "fragments/components/employee-list :: employees";
    //endregion

    //region GET
    @GetMapping("/manage-employees")
    @ResponseBody
    public ModelAndView getEmployees() {

        ModelAndView modelAndView = addEmployeeListToView(VIEW_MANAGE_EMPLOYEES);

        List<Department> departments = getDepartmentService().getAllActive();
        modelAndView.addObject("departmentList", departments);
        modelAndView.addObject("active_page_manage", "active");
        return modelAndView;
    }

    @GetMapping(value = {
            "/edit-employee/{id}"})
    public ModelAndView editEmployee(@PathVariable("id") String id) {
        ModelAndView modelAndView = new ModelAndView(VIEW_EDIT_EMPLOYEE);
        try {
            Long employeeId = Long.valueOf(id);
            Optional<Employee> employee = getEmployeeService().findById(employeeId);
            if (employee.isPresent()) {
                EmployeeDataMapper employeeDataMapper = new EmployeeDataMapper();
                EmployeeDTO employeeDTO = employeeDataMapper.reverseMap(employee.get());
                modelAndView.addObject("employee", employeeDTO);
            }

            List<Department> departments = getDepartmentService().getAllActive();
            modelAndView.addObject("departmentList", departments);

        } catch (ServiceException e) {
            logErrorAndSetModel(logger, "editEmployee", "An error occurred while trying to show employee to edit", e, modelAndView);
        }
        return modelAndView;
    }

    @GetMapping("/employee-list")
    @ResponseBody
    public ModelAndView getEmployeeList() {
        return addEmployeeListToView(VIEW_EMPLOYEES_TABLE);
    }
    //endregion

    //region POST
    @PostMapping("/add-employee")
    @ResponseBody
    public ResponseEntity<Response<Employee>> addNewEmployee(@RequestBody EmployeeDTO employeeDTO) {
        try {
            if (!DataHelper.validateEmployeeData(employeeDTO)) {
                return prepareResponseEntityWithData(OPERATION_CODE_WARNING,
                        "Employee information is incomplete!",
                        "Employee information is incomplete! <br/> Please fill the required field before proceeding.",
                        HttpStatus.OK);
            }
            EmployeeDataMapper employeeDataMapper = new EmployeeDataMapper(new Employee());
            Optional<Department> department = getDepartmentService().findById(employeeDTO.getDepartmentId());

            Employee employeeToUpdate = employeeDataMapper.map(employeeDTO);
            department.ifPresent(employeeToUpdate::setDepartment);

            Employee employee = getEmployeeService().save(employeeToUpdate);
            return prepareResponseEntityWithData(OPERATION_CODE_SUCCESS,
                    "Employee created successfully",
                    "Employee was created successfully!",
                    employee,
                    HttpStatus.OK);
        } catch (ServiceException e) {
            e.printStackTrace();
            return logAndPrepareResponseEntityForException(logger, "addNewEmployee", e);
        }
    }

    @PostMapping("/edit-employee")
    @ResponseBody
    public ResponseEntity<Response<Employee>> editEmployee(@RequestBody EmployeeDTO employeeDTO) {
        try {
            if (!DataHelper.validateEmployeeData(employeeDTO)) {
                return prepareResponseEntityWithData(OPERATION_CODE_WARNING,
                        "Employee information is incomplete!",
                        "Employee information is incomplete! <br/> Please fill the required field before proceeding.",
                        HttpStatus.OK);
            }
            Optional<Employee> employee = getEmployeeService().findById(employeeDTO.getId());

            if (employee.isPresent()) {
                EmployeeDataMapper employeeDataMapper = new EmployeeDataMapper(employee.get());
                Employee employeeToUpdate = employeeDataMapper.map(employeeDTO);

                Optional<Department> employeeDepartment = getDepartmentService().findById(employeeDTO.getDepartmentId());

                if (employeeDepartment.isPresent()) {
                    employeeToUpdate.setDepartment(employeeDepartment.get());
                } else {
                    employeeToUpdate.setDepartment(null);
                }
                getEmployeeService().updateEmployeeInformation(employeeToUpdate);
            }
            return prepareResponseEntityWithData(OPERATION_CODE_SUCCESS,
                    "Employee updated successfully!",
                    "Employee was updated successfully!",
                    HttpStatus.OK);
        } catch (ServiceException e) {
            e.printStackTrace();
            return logAndPrepareResponseEntityForException(logger, "editEmployee", e);
        }
    }

    @PostMapping("/delete-employee")
    @ResponseBody
    public ResponseEntity<Response<Employee>> deleteEmployee(@RequestParam("employeeId") String employeeId) {
        try {
            if (StringUtils.isEmpty(employeeId)) {
                return prepareResponseEntityWithData(OPERATION_CODE_WARNING,
                        "Employee id found",
                        "Error! Employee id was not found!",
                        HttpStatus.OK);
            }
            Long id = Long.valueOf(employeeId);
            Optional<Employee> employeeToDelete = getEmployeeService().findById(id);

            if (!employeeToDelete.isPresent()) {
                return prepareResponseEntityWithData(OPERATION_CODE_ERROR,
                        "Employee not found",
                        "Error! Employee was not found!",
                        HttpStatus.OK);
            }
            getEmployeeService().deleteEmployee(employeeToDelete.get(), true);

            return prepareResponseEntityWithData(OPERATION_CODE_SUCCESS,
                    "Employee deleted successfully!",
                    "Employee was deleted successfully!",
                    null,
                    HttpStatus.OK);
        } catch (ServiceException e) {
            e.printStackTrace();
            return logAndPrepareResponseEntityForException(logger, "deleteEmployee", e);
        }
    }
    //end region

    private ModelAndView addEmployeeListToView(String viewName) {
        ModelAndView modelAndView = new ModelAndView(viewName);
        try {
            List<Employee> employeeList = getEmployeeService().findAll();
            modelAndView.addObject("employeeList", employeeList);
        } catch (Exception e) {
            logErrorAndSetModel(logger, "getEmployees", "An error occurred while trying to show employees", e, modelAndView);
        }
        return modelAndView;
    }
}
