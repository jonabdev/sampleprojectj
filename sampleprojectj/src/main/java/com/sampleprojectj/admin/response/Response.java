package com.sampleprojectj.admin.response;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Response<T> {
    private T item;
    private int code;
    private String message;
    private String detail;

    public Response() {}

    private Response(Builder builder) {
        setItem((T) builder.item);
        setCode(builder.code);
        setMessage(builder.message);
        setDetail(builder.detail);
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Response response = (Response) o;

        return new EqualsBuilder()
                .append(code, response.code)
                .append(message, response.message)
                .append(detail, response.detail)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(code)
                .append(message)
                .append(detail)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("code", code)
                .append("message", message)
                .append("detail", detail)
                .toString();
    }


    public static final class Builder<T> {
        private T item;
        private int code;
        private String message;
        private String detail;

        public Builder() {
        }

        public Builder item(T val) {
            item = val;
            return this;
        }

        public Builder code(int val) {
            code = val;
            return this;
        }

        public Builder message(String val) {
            message = val;
            return this;
        }

        public Builder detail(String val) {
            detail = val;
            return this;
        }

        public Response build() {
            return new Response(this);
        }
    }
}
