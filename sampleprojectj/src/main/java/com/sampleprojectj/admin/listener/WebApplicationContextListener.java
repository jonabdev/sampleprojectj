package com.sampleprojectj.admin.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class WebApplicationContextListener implements ServletContextListener {

    private static Logger logger = LogManager.getLogger(WebApplicationContextListener.class);

    private static final String WEBAPP_INFO = "SampleProjectJ";

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.info(WEBAPP_INFO.concat(" / Context Initialized"));
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        logger.info(WEBAPP_INFO.concat(" / Context Destroyed"));
    }
}
