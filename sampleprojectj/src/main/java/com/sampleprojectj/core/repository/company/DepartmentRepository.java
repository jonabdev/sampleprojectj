package com.sampleprojectj.core.repository.company;

import com.sampleprojectj.core.entity.company.Department;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
    List<Department> findAllByActive(boolean active);
}
