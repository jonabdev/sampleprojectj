package com.sampleprojectj.core.repository.user;

import com.sampleprojectj.core.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    Optional<User> findById(Long id);

    List<User> findAllByActive(boolean active);

    @Query("select u from User u where u.email = ?1 and u.active = true")
    Optional<User> getActiveUserByEmail(String userName);
}
