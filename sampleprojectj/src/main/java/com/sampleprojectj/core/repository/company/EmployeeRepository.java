package com.sampleprojectj.core.repository.company;

import com.sampleprojectj.core.entity.company.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    List<Employee> findAllByActive(boolean active);

    @Modifying
    @Query("update Employee e set e.active = false where e.id = :id")
    void deleteEmployee(@Param("id") Long id);
}
