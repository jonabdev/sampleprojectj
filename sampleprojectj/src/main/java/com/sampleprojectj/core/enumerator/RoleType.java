package com.sampleprojectj.core.enumerator;

public enum RoleType {
    ADMIN(1),
    USER(2);

    private Integer code;

    RoleType(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public static RoleType fromCode(Integer code) {
        for (RoleType RoleType : RoleType.values()) {
            if (RoleType.getCode().equals(code)) {
                return RoleType;
            }
        }
        throw new UnsupportedOperationException("Enumeration Code " + code + " is not supported");
    }
    
}
