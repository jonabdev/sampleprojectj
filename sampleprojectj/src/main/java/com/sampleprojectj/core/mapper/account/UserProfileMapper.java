package com.sampleprojectj.core.mapper.account;

import com.sampleprojectj.core.dto.UserProfileDTO;
import com.sampleprojectj.core.entity.user.User;
import com.sampleprojectj.core.mapper.base.Mapper;
import org.apache.commons.lang3.NotImplementedException;

public class UserProfileMapper extends Mapper<UserProfileDTO, User> {

    private User user;

    public UserProfileMapper(User user) {
        this.user = user;
    }

    @Override
    public User map(UserProfileDTO obj) {

        User user = getUser();

        user.setFirstName(obj.getFirstName());
        user.setLastName(obj.getLastName());
        user.setEmail(obj.getEmail().toLowerCase());

        return user;
    }

    @Override
    public UserProfileDTO reverseMap(User obj) {
        throw new NotImplementedException("Operation is not allowed");
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
