package com.sampleprojectj.core.mapper.company;

import com.sampleprojectj.core.dto.EmployeeDTO;
import com.sampleprojectj.core.entity.company.Employee;
import com.sampleprojectj.core.mapper.base.Mapper;

public class EmployeeDataMapper extends Mapper<EmployeeDTO, Employee> {

    private Employee employee;

    public EmployeeDataMapper(){

    }
    public EmployeeDataMapper(Employee employee) {
        this.employee = employee;
    }

    @Override
    public Employee map(EmployeeDTO item) {
        Employee employee = getEmployee();

        employee.setName(item.getName());
        employee.setAddress(item.getAddress());
        employee.setPosition(item.getPosition());
        employee.setActive(item.isActive());

        return employee;
    }

    @Override
    public EmployeeDTO reverseMap(Employee item) {
        EmployeeDTO employeeDTO = new EmployeeDTO();

        employeeDTO.setId(item.getId());
        employeeDTO.setName(item.getName());
        employeeDTO.setAddress(item.getAddress());
        employeeDTO.setPosition(item.getPosition());

        if(item.getDepartment() != null) {
            employeeDTO.setDepartmentId(item.getDepartment().getId());
            employeeDTO.setDepartmentName(item.getDepartment().getName());
        }
        employeeDTO.setActive(item.isActive());
        return employeeDTO;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
