package com.sampleprojectj.core.mapper.base;

import java.util.ArrayList;
import java.util.List;

public abstract class Mapper<T1, T2> {

    public abstract T2 map(T1 item);

    public abstract T1 reverseMap(T2 item);

    public List<T2> map(List<T1> items) {
        List<T2> returnItems = new ArrayList<>(items.size());
        for (T1 item : items) {
            returnItems.add(map(item));
        }
        return returnItems;
    }

    public List<T1> reverseMap(List<T2> items) {
        List<T1> returnItems = new ArrayList<>(items.size());
        for (T2 item : items) {
            returnItems.add(reverseMap(item));
        }
        return returnItems;
    }
}
