package com.sampleprojectj.core.constants;

public class ApplicationConstants {

    public static final String GENERIC_ERROR = "An error occurred while processing your request, please try again";

    public static final String SECURITY_GENERIC_ERROR = "User credentials not found, please try again";
    public static final String SECURITY_INVALID_PARAMETERS_ERROR = "Invalid parameters, please try again";
    public static final String SECURITY_INVALID_PASSWORD_ERROR = "Invalid password";

    public static final String WEBAPP_ADMIN_USER_DB = "webapp-admin";
    public static final String WEBAPP_ADMIN_USER_SIMPLE = "webapp-user";

}
