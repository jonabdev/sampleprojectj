package com.sampleprojectj.core.entity.base;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sampleprojectj.core.serializer.JsonDateTimeISO8601Deserializer;
import com.sampleprojectj.core.serializer.JsonDateTimeISO8601Serializer;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Calendar;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonIgnoreProperties(value = "$$_hibernate_interceptor")
@MappedSuperclass
public abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @JsonSerialize(using = JsonDateTimeISO8601Serializer.class)
    @JsonDeserialize(using = JsonDateTimeISO8601Deserializer.class)
    @JsonProperty(value = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "insert_date", nullable = false)
    private Calendar insertDate;

    @JsonProperty(value = "insert_username")
    @Column(name = "insert_username", nullable = false)
    private String insertUsername;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Calendar getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Calendar insertDate) {
        this.insertDate = insertDate;
    }

    public String getInsertUsername() {
        return insertUsername;
    }

    public void setInsertUsername(String insertUsername) {
        this.insertUsername = insertUsername;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        BaseEntity that = (BaseEntity) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(insertDate, that.insertDate)
                .append(insertUsername, that.insertUsername)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(insertDate)
                .append(insertUsername)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("insertDate", insertDate)
                .append("insertUsername", insertUsername)
                .toString();
    }
}
