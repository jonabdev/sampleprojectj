package com.sampleprojectj.core.entity.base;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sampleprojectj.core.serializer.JsonDateTimeISO8601Deserializer;
import com.sampleprojectj.core.serializer.JsonDateTimeISO8601Serializer;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.Calendar;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@MappedSuperclass
public abstract class BaseManagedEntity extends BaseEntity {

    @Column(name = "active", nullable = false)
    private boolean active;

    @JsonSerialize(using = JsonDateTimeISO8601Serializer.class)
    @JsonDeserialize(using = JsonDateTimeISO8601Deserializer.class)
    @JsonProperty(value = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_date")
    private Calendar updateDate;

    @JsonProperty(value = "update_username")
    @Column(name = "update_username")
    private String updateUsername;

    @JsonIgnore
    @Version
    @Column(name = "row_version")
    private Long version;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Calendar getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Calendar updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUsername() {
        return updateUsername;
    }

    public void setUpdateUsername(String updateUsername) {
        this.updateUsername = updateUsername;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        BaseManagedEntity that = (BaseManagedEntity) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(active, that.active)
                .append(updateDate, that.updateDate)
                .append(updateUsername, that.updateUsername)
                .append(version, that.version)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(active)
                .append(updateDate)
                .append(updateUsername)
                .append(version)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("active", active)
                .append("updateDate", updateDate)
                .append("updateUsername", updateUsername)
                .append("version", version)
                .toString();
    }
}
