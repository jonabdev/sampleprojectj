package com.sampleprojectj.core.service.company;

import com.sampleprojectj.core.entity.company.Employee;
import com.sampleprojectj.core.exception.ServiceException;
import com.sampleprojectj.core.service.base.CrudService;

public interface EmployeeService extends CrudService<Employee> {

    Employee updateEmployeeInformation(Employee employee) throws ServiceException;
	
	void deleteEmployee(Employee employee, boolean isPermanentAction) throws ServiceException;
}
