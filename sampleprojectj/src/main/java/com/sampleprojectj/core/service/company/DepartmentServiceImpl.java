package com.sampleprojectj.core.service.company;

import com.sampleprojectj.core.entity.company.Department;
import com.sampleprojectj.core.exception.ServiceException;
import com.sampleprojectj.core.repository.company.DepartmentRepository;
import com.sampleprojectj.core.service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class DepartmentServiceImpl extends BaseService implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public Department save(Department entity) throws ServiceException {
        return null;
        //TODO
    }

    @Override
    public void delete(Department entity) throws ServiceException {
        //TODO
    }

    @Override
    public Optional<Department> findById(Long id) throws ServiceException {
        return departmentRepository.findById(id);
    }

    @Override
    public List<Department> findAll() throws ServiceException {
        return departmentRepository.findAll();
    }

    @Override
    public List<Department> getAllActive() throws ServiceException {
        return departmentRepository.findAllByActive(true);
    }
}
