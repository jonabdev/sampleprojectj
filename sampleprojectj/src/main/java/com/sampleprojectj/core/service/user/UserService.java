package com.sampleprojectj.core.service.user;

import com.sampleprojectj.core.entity.user.Role;
import com.sampleprojectj.core.entity.user.User;
import com.sampleprojectj.core.enumerator.RoleType;
import com.sampleprojectj.core.exception.ServiceException;
import com.sampleprojectj.core.service.base.CrudService;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;

public interface UserService extends CrudService<User>, UserDetailsService {

    Optional<User> getUserByEmail(String email) throws ServiceException;

    User save(User user, List<RoleType> roleTypeList) throws ServiceException;

    User updateUserInformation(User user) throws ServiceException;

    Optional<Role> getRole(String name) throws ServiceException;

    Optional<User> login(String email, String password) throws ServiceException;

}
