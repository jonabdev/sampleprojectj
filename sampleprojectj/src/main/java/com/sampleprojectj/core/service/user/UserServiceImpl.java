package com.sampleprojectj.core.service.user;

import com.sampleprojectj.core.constants.ApplicationConstants;
import com.sampleprojectj.core.entity.user.Role;
import com.sampleprojectj.core.entity.user.User;
import com.sampleprojectj.core.enumerator.RoleType;
import com.sampleprojectj.core.exception.ServiceException;
import com.sampleprojectj.core.repository.user.RoleRepository;
import com.sampleprojectj.core.repository.user.UserRepository;
import com.sampleprojectj.core.service.base.BaseService;
import com.sampleprojectj.core.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UserServiceImpl extends BaseService implements UserService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Optional<User> getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Optional<Role> getRole(String email) {
        return roleRepository.findByName(email);
    }

    @Override
    public User updateUserInformation(User user) throws ServiceException {
        user.setUpdateDate(Util.getCurrentDate());
        user.setUpdateUsername(user.getEmail());
        return userRepository.save(user);
    }

    @Override
    public User save(User user, List<RoleType> roleTypeList) throws ServiceException {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        Collection<Role> roles = new HashSet<>();
        roleTypeList.stream().forEach(roleType -> roleRepository.findById(roleType.getCode().longValue()).ifPresent(roles::add));

        if (CollectionUtils.isEmpty(roles)) {
            throw new InsufficientAuthenticationException("There is not a valid role");
        }
        user.setAuthorities(roles);

        user.setActive(true);
        user.setInsertDate(Util.getCurrentDate());
        user.setInsertUsername(user.getEmail());
        return userRepository.save(user);
    }

    @Override
    public User save(User entity) throws ServiceException {
        //TODO
        return null;
    }

    @Override
    public void delete(User entity) throws ServiceException {
    }

    @Override
    public Optional<User> findById(Long id) throws ServiceException {
        return userRepository.findById(id);
    }

    @Override
    public List<User> findAll() throws ServiceException {
        return userRepository.findAll();
    }

    @Override
    public List<User> getAllActive() throws ServiceException {
        return userRepository.findAllByActive(true);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = authenticateUser(username);

        //Validate user authority
        Collection<Role> authorities = user.get().getAuthorities();
        if (CollectionUtils.isEmpty(authorities)) {
            throw new InsufficientAuthenticationException("There is not a valid role");
        }
        return user.get();
    }

    @Override
    public Optional<User> login(String email, String password) throws ServiceException {
        try {
            Optional<User> user = authenticateUser(email);

            if (!bCryptPasswordEncoder.matches(password, user.get().getPassword())) {
                throw new UsernameNotFoundException(ApplicationConstants.SECURITY_GENERIC_ERROR);
            }
            //Validate user authority
            Collection<Role> authorities = user.get().getAuthorities();
            if (CollectionUtils.isEmpty(authorities)) {
                throw new InsufficientAuthenticationException("There is not a valid role");
            }
            if (authorities.stream().noneMatch(role ->
                    role.getId().equals(RoleType.ADMIN.getCode().longValue())
                            || role.getId().equals(RoleType.USER.getCode().longValue()))) {
                throw new InsufficientAuthenticationException("There is not a valid role");
            }
            return user;

        } catch (UsernameNotFoundException ex) {
            return Optional.empty();
        }
    }

    private Optional<User> authenticateUser(String email) {
        Optional<User> user = userRepository.getActiveUserByEmail(email);
        if (!user.isPresent()) {
            throw new UsernameNotFoundException(ApplicationConstants.SECURITY_GENERIC_ERROR);
        }

        if (user.get().getId() == null || user.get().getId() == 0) {
            throw new UsernameNotFoundException(ApplicationConstants.SECURITY_GENERIC_ERROR);
        }
        return user;
    }
}

