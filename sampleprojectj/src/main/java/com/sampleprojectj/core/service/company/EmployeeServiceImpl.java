package com.sampleprojectj.core.service.company;

import com.sampleprojectj.core.constants.ApplicationConstants;
import com.sampleprojectj.core.entity.company.Employee;
import com.sampleprojectj.core.exception.ServiceException;
import com.sampleprojectj.core.repository.company.EmployeeRepository;
import com.sampleprojectj.core.service.base.BaseService;
import com.sampleprojectj.core.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class EmployeeServiceImpl extends BaseService implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee save(Employee employee) throws ServiceException {
        employee.setInsertDate(Util.getCurrentDate());
        employee.setInsertUsername(ApplicationConstants.WEBAPP_ADMIN_USER_DB);
        return employeeRepository.save(employee);
    }

    @Override
    public Employee updateEmployeeInformation(Employee employee) throws ServiceException {
        employee.setUpdateDate(Util.getCurrentDate());
        employee.setUpdateUsername(ApplicationConstants.WEBAPP_ADMIN_USER_DB);
        return employeeRepository.save(employee);
    }

    @Override
    public void delete(Employee employee) throws ServiceException {
    }
	
	@Override
    public void deleteEmployee(Employee employee, boolean isPermanentAction) throws ServiceException {
		if(isPermanentAction){
            employeeRepository.delete(employee);
		}else{
            employeeRepository.deleteEmployee(employee.getId()); // makes entity inactive
		}
    }

    @Override
    public Optional<Employee> findById(Long id) throws ServiceException {
        return employeeRepository.findById(id);
    }

    @Override
    public List<Employee> findAll() throws ServiceException {
        return employeeRepository.findAll();
    }

    @Override
    public List<Employee> getAllActive() throws ServiceException {
        return employeeRepository.findAllByActive(true);
    }
}
