package com.sampleprojectj.core.service.company;

import com.sampleprojectj.core.entity.company.Department;
import com.sampleprojectj.core.service.base.CrudService;

public interface DepartmentService extends CrudService<Department> {

}
