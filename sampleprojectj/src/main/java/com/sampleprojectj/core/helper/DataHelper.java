package com.sampleprojectj.core.helper;

import com.sampleprojectj.core.dto.EmployeeDTO;
import com.sampleprojectj.core.dto.UserProfileDTO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.passay.*;

import java.util.Arrays;

public class DataHelper {

    public static boolean validateUserLoginData(String email, String password) {
        if (StringUtils.isEmpty(email)) {
            return false;
        }
        if (StringUtils.isEmpty(password)) {
            return false;
        }
        return true;
    }

    public static boolean validateUserProfileData(UserProfileDTO userProfileDTO) {
        if (userProfileDTO == null) {
            return false;
        }
        if (StringUtils.isEmpty(userProfileDTO.getEmail())) {
            return false;
        }
        return true;
    }

    public static boolean validateEmployeeData(EmployeeDTO employeeDTO) {
        if (employeeDTO == null) {
            return false;
        }

        if (StringUtils.isEmpty(employeeDTO.getName())) {
            return false;
        }

        return true;
    }

    public static boolean isEmailValid(String email) {
        return EmailValidator.getInstance().isValid(email);
    }

    public static boolean isPasswordValid(String password) {
        PasswordValidator passwordValidator = new PasswordValidator(Arrays.asList(
                new LengthRule(8, 20),
                new UppercaseCharacterRule(1),
                new DigitCharacterRule(1),
                new QwertySequenceRule(6, false),
                new AlphabeticalSequenceRule(6, false),
                new WhitespaceRule()
        ));
        return passwordValidator
                .validate(new PasswordData(password))
                .isValid();
    }

    public static boolean checkNameHasRegularCharactersOnly(String name) {
        String pattern = "^((?=[A-Za-z0-9@])(?![_\\\\-]).)*$";
        return name.matches(pattern);
    }

    private static String removeSpaces(String str) {
        return str.replaceAll("\\s+", "");
    }

}
