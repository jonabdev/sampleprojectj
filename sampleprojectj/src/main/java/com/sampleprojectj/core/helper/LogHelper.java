package com.sampleprojectj.core.helper;

import org.apache.logging.log4j.Logger;

public class LogHelper {

    public static void logInfo(Logger logger, String operation, String message) {
        logger.info(operation + " - " + message);
    }

    public static void logWarn(Logger logger, String operation, String message) {
        logger.warn(operation + " - " + message);
    }


    public static void logError(Logger logger, String operation, Exception e) {
        logError(logger, operation, "An error occurred while trying to execute the operation " + operation, e);
    }

    public static void logError(Logger logger, String operation, String message, Exception e) {
        if (e == null) {
            logger.error(operation + " - " + message);
            return;
        }
        logger.error(operation + " - " + message + " ERROR: ", e.getMessage());
        if (e.getCause() != null) {
            logger.error("CAUSE N1: " + e.getCause().getMessage());

            if (e.getCause().getCause() != null) {
                logger.error("CAUSE N2: " + e.getCause().getCause().getMessage());
            }
        }
    }
}
