package com.sampleprojectj.core.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class Util {

    public static final String DATE_SIMPLE = "dd/MM/yyyy";
    public static final String DATETIME_SIMPLE = "dd/MM/yyyy HH:mm";

    public static final String DATE_ISO_8601 = "yyyy-MM-dd";
    public static final String DATETIME_ISO_8601 = "yyyy-MM-dd'T'HH:mm:ss";

    public static SimpleDateFormat getDateSimple() {
        SimpleDateFormat SDF_ISO_8601_DATE = new SimpleDateFormat(DATE_SIMPLE);
        SDF_ISO_8601_DATE.setLenient(false);
        return SDF_ISO_8601_DATE;
    }

    public static SimpleDateFormat getDateTimeSimple() {
        SimpleDateFormat SDF_ISO_8601_DATETIME = new SimpleDateFormat(DATETIME_SIMPLE);
        SDF_ISO_8601_DATETIME.setLenient(false);
        return SDF_ISO_8601_DATETIME;
    }

    public static SimpleDateFormat getDateFormatIso8601() {
        SimpleDateFormat SDF_ISO_8601_DATE = new SimpleDateFormat(DATE_ISO_8601);
        SDF_ISO_8601_DATE.setLenient(false);
        return SDF_ISO_8601_DATE;
    }

    public static SimpleDateFormat getDateTimeFormatIso8601() {
        SimpleDateFormat SDF_ISO_8601_DATETIME = new SimpleDateFormat(DATETIME_ISO_8601);
        SDF_ISO_8601_DATETIME.setLenient(false);
        return SDF_ISO_8601_DATETIME;
    }

    public static Calendar getCurrentDate() {
        return new GregorianCalendar();
    }

    public static Calendar getDate(long date) {
        Calendar setDate = getCurrentDate();
        setDate.setTime(new Date(date));
        return setDate;
    }
}
