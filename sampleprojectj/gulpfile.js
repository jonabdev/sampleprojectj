var gulp = require('gulp');
var sass = require('gulp-sass');
var clean = require('gulp-clean');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var prefix = require('gulp-autoprefixer');

var prefixerOptions = {
    browsers: ['cover 99.5%']
};

//Clean
gulp.task('clean', function () {
    return gulp.src('src/main/webapp/WEB-INF/static/css/build.css')
        .pipe(clean({force: true}));
});

//Styles
gulp.task('styles', function (done) {
    gulp.src('src/main/webapp/WEB-INF/static/scss/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(prefix(prefixerOptions))
        .pipe(rename('build.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('src/main/webapp/WEB-INF/static/css/'));
    done();
});

//Default Task - RUN
gulp.task('default', gulp.series('clean', 'styles'));
